var classDeuteronEventGen =
[
    [ "DeuteronEventGen", "classDeuteronEventGen.html#a492917dff043a0f39ee0ce9c0e1dd03b", null ],
    [ "~DeuteronEventGen", "classDeuteronEventGen.html#a2d5a86a4089db5c47cf6f4f89f5a9bd5", null ],
    [ "ClassDef", "classDeuteronEventGen.html#a9f1b891535c5330268cb23c4be13211e", null ],
    [ "Generate", "classDeuteronEventGen.html#a248a361998610b56da73a9e36fb3de49", null ],
    [ "Generate", "classDeuteronEventGen.html#ab1f1794f65e710138df1789c26774ca3", null ],
    [ "GenerateSingleNucleon", "classDeuteronEventGen.html#afcbe0dc135629d05458daedc7870bd6a", null ],
    [ "GetChannels", "classDeuteronEventGen.html#a417c98e795c3acfa89854cccff1ca0e7", null ],
    [ "StartPosition", "classDeuteronEventGen.html#a2add648dc06a3a92a14b6d6e8bcb7483", null ],
    [ "StartPosition2", "classDeuteronEventGen.html#a75e5f02e90f0b038b0fbb915a214743b", null ],
    [ "TotalCrossSection", "classDeuteronEventGen.html#a3f6b65f44040ba2ad889761c87a36bd7", null ],
    [ "channels", "classDeuteronEventGen.html#a538c285bce5de9522a2522c6de9612c4", null ],
    [ "tcs", "classDeuteronEventGen.html#ae27cf562c22af8ac64243a3da5c2d3b1", null ]
];