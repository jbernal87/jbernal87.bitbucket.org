var structCascadeData =
[
    [ "CascadeData", "structCascadeData.html#aa4573166c297fd6ea0cb768c6870f034", null ],
    [ "A", "structCascadeData.html#a56a52f73797a8d06045cf5bc1ab831bf", null ],
    [ "Energies", "structCascadeData.html#abc6619f669cb343c95207413d7596f5d", null ],
    [ "ExpData", "structCascadeData.html#a39d05993313c3de083f52751256ebc21", null ],
    [ "ExpMax", "structCascadeData.html#a71f1bdb45496a8abe30665133fc36dc4", null ],
    [ "ExpMin", "structCascadeData.html#a809f5a88acbab6ab97ae71730783567c", null ],
    [ "Graphs", "structCascadeData.html#a496e79a7e795cea14182a1838865d4e4", null ],
    [ "N", "structCascadeData.html#a7722c8ecbb62d99aee7ce68b1752f337", null ],
    [ "NucleusName", "structCascadeData.html#ae51cb650ede63c01b3a911453177b395", null ],
    [ "Results", "structCascadeData.html#acaafc27b83984767aa36c4a689f1cb8c", null ],
    [ "Type", "structCascadeData.html#a6fcef4329ae329f0b7b6b8d739be7121", null ],
    [ "Z", "structCascadeData.html#a5ed5bfe6933ed8cba853237650cc041b", null ]
];