var bb__actions_8cc =
[
    [ "bb_elastic", "bb__actions_8cc.html#a329cee94a85e1f88d6d995a54f5b2075", null ],
    [ "bb_elastic_np", "bb__actions_8cc.html#a3282af2d96963aeb9590f1374ffb3037", null ],
    [ "bb_inelastic", "bb__actions_8cc.html#aced73181b6f64ac802d7473482a54f4f", null ],
    [ "elastic_np_angle", "bb__actions_8cc.html#afa7bd1ca176117e33bfb6df17c429cb0", null ],
    [ "NDelta_inelastic", "bb__actions_8cc.html#aee47e445a0d9938ac6ab74e0e99c4cca", null ],
    [ "NLambda_inelastic", "bb__actions_8cc.html#a736051ba1b5b322e736bd0ff2e381449", null ],
    [ "NN_inelastic", "bb__actions_8cc.html#a50209a09de7828414bf19a59471946b4", null ],
    [ "np_inelastic", "bb__actions_8cc.html#af3537b5dca1af9d4a05739dbd0d21d9c", null ]
];