var structelements__rootFiles =
[
    [ "A", "structelements__rootFiles.html#a56a52f73797a8d06045cf5bc1ab831bf", null ],
    [ "alphas", "structelements__rootFiles.html#a17823eedd1347278ee4358d485d8ec1a", null ],
    [ "energy", "structelements__rootFiles.html#a1d26cc49e49afe96406210d6beaf418a", null ],
    [ "fissility", "structelements__rootFiles.html#a3bee39017119d3325a31d61defc76ee3", null ],
    [ "fission", "structelements__rootFiles.html#a6f7b7c4ed1f7e8294f21fae1557a0218", null ],
    [ "l", "structelements__rootFiles.html#a59e80b8ba32c12c6d0a868f17a19ae48", null ],
    [ "neutrons", "structelements__rootFiles.html#a29501091f104a35b97e7bf251906d9a1", null ],
    [ "protons", "structelements__rootFiles.html#abe04d945af71c67ae6785dc46245116d", null ],
    [ "q", "structelements__rootFiles.html#a5b5e3f03e443adea974601f295136638", null ],
    [ "qes", "structelements__rootFiles.html#a605772968d2cf8c53564bdd7661ac9cd", null ],
    [ "Z", "structelements__rootFiles.html#a5ed5bfe6933ed8cba853237650cc041b", null ]
];