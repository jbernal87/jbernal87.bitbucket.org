var classBremsstrahlungEventGen =
[
    [ "BremsstrahlungEventGen", "classBremsstrahlungEventGen.html#ab7dbbe80dadb606789b6eb9757ffdd87", null ],
    [ "~BremsstrahlungEventGen", "classBremsstrahlungEventGen.html#a69467e5aa38ec3e692223373ffdf64bd", null ],
    [ "ClassDef", "classBremsstrahlungEventGen.html#abeffb8e94bad58848326fc80a6ae6c01", null ],
    [ "Generate", "classBremsstrahlungEventGen.html#aac201211488c320c072777401de33343", null ],
    [ "Generate", "classBremsstrahlungEventGen.html#a4e05de0f17c97799888f2180cf8fdac6", null ],
    [ "Generate", "classBremsstrahlungEventGen.html#aa1669a3c480efaea4eea894f2ce5f287", null ],
    [ "GetChannels", "classBremsstrahlungEventGen.html#a417c98e795c3acfa89854cccff1ca0e7", null ],
    [ "NumPhotons", "classBremsstrahlungEventGen.html#a3884f1ff7e849dfe6031cb36bd8c0d2b", null ],
    [ "ResetNumPhotons", "classBremsstrahlungEventGen.html#afd650f6255c28d2afe19bcacfdf6839b", null ],
    [ "SelectNucleons", "classBremsstrahlungEventGen.html#a3f5e153e8a6a0ff06f210093d28a6c77", null ],
    [ "StartPosition", "classBremsstrahlungEventGen.html#a2add648dc06a3a92a14b6d6e8bcb7483", null ],
    [ "TotalCrossSection", "classBremsstrahlungEventGen.html#afabcb3afc94ed8ad374289df0ace6bba", null ],
    [ "channels", "classBremsstrahlungEventGen.html#a538c285bce5de9522a2522c6de9612c4", null ],
    [ "num_photons", "classBremsstrahlungEventGen.html#a83b5e285fd750cf0c3c2c30b2d370c55", null ],
    [ "selected_ch", "classBremsstrahlungEventGen.html#a942d30c5c6ae73bda2b89e1ab97fd97a", null ],
    [ "selected_nucleon", "classBremsstrahlungEventGen.html#a6ea68b9c838b24e9e8258154e84f9ce5", null ],
    [ "tcs", "classBremsstrahlungEventGen.html#ae27cf562c22af8ac64243a3da5c2d3b1", null ]
];