var Lepton__n__cross__sections_8hh =
[
    [ "nun_elastic_cross_section", "Lepton__n__cross__sections_8hh.html#a7b9aa2592444de2ee5f13c0ecc1651f5", null ],
    [ "nun_mump33p_cross_section", "Lepton__n__cross__sections_8hh.html#a9a37d0e46e2ae6cad64680907e015805", null ],
    [ "nun_mump_cross_section", "Lepton__n__cross__sections_8hh.html#a7ce1c2cbb0281801628fb54a9abd6fbf", null ],
    [ "nun_nup330_cross_section", "Lepton__n__cross__sections_8hh.html#a80e7c82a6c94aa21fff6f058ab7b28e3", null ],
    [ "nup_elastic_cross_section", "Lepton__n__cross__sections_8hh.html#ab350677da707d92dc50fc5f63cbeef62", null ],
    [ "nup_mump33pp_cross_section", "Lepton__n__cross__sections_8hh.html#a065854bec554c389de589f25b67f1504", null ],
    [ "nup_nup33p_cross_section", "Lepton__n__cross__sections_8hh.html#ad2f69f0daf54c948513d844325f4e354", null ],
    [ "factor", "Lepton__n__cross__sections_8hh.html#a3c259aceb65bcd94038c8cac5c5932c1", null ]
];