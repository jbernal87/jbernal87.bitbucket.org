var structProc__Struct =
[
    [ "CPT_Print", "structProc__Struct.html#ae473b77529066a098700d3eafa0088ec", null ],
    [ "Print", "structProc__Struct.html#a9dcac18006ce057b8d78c847174c1362", null ],
    [ "I1", "structProc__Struct.html#a01d49674751f9b048335758bc49f6612", null ],
    [ "I2", "structProc__Struct.html#ab62a85ec9a71c91f58d3d23f4d72785f", null ],
    [ "NPartIn", "structProc__Struct.html#a7b82c1c9f15c2db8468642efc276aa15", null ],
    [ "NTotal", "structProc__Struct.html#a041b1a57fd1b739e9dd65259d28e0511", null ],
    [ "Proc_E", "structProc__Struct.html#aa8820213be145c26962eea48e6d3d3e1", null ],
    [ "Proc_IsB", "structProc__Struct.html#a4724fc1ab4b916024a8d0f7c0a57e6c0", null ],
    [ "Proc_IsF", "structProc__Struct.html#a5678a0fa0d5cce6697f6e8bee80cc9ce", null ],
    [ "Proc_K", "structProc__Struct.html#ade4d818a3b2866507569a72ca45aebae", null ],
    [ "Proc_P", "structProc__Struct.html#a0220c1437612dfb5695b0c4a11ced3f0", null ],
    [ "Proc_PID", "structProc__Struct.html#a50426284474a361da5172af623645390", null ],
    [ "Proc_PX", "structProc__Struct.html#aade4d60dafd48c261e64ecd7f8902a8e", null ],
    [ "Proc_PY", "structProc__Struct.html#a6b345b469ed1358082d5bde4fc4f4084", null ],
    [ "Proc_PZ", "structProc__Struct.html#a3790d703ebc7183123a5c47a8305d7a4", null ],
    [ "Proc_X", "structProc__Struct.html#a10001849453f48e8385ddf961dda7408", null ],
    [ "Proc_Y", "structProc__Struct.html#a4bb0bbaeb6b451e75ea4ee9d7874b3d0", null ],
    [ "Proc_Z", "structProc__Struct.html#abab4233c61412cb7d9abadae8d441d08", null ],
    [ "ProN", "structProc__Struct.html#adacb9ecd7e9fd05a6f717283c64bc72a", null ],
    [ "TIME", "structProc__Struct.html#a15f6a1655b7ce87c9ed1644b92cd45cb", null ]
];