var classProtonEventGen =
[
    [ "ProtonEventGen", "classProtonEventGen.html#a85f98464221d2f76779f75b9eb08b85f", null ],
    [ "~ProtonEventGen", "classProtonEventGen.html#ad9ac64a02876955ebef1be9562505e9e", null ],
    [ "ClassDef", "classProtonEventGen.html#a8b7951b39119e8c07c09476890ff659c", null ],
    [ "Generate", "classProtonEventGen.html#aeaf864e2b7cab5a59fdf1a5e367b4871", null ],
    [ "Generate", "classProtonEventGen.html#a85e5088eb8507d0dd711e5efd4912799", null ],
    [ "GetChannels", "classProtonEventGen.html#a417c98e795c3acfa89854cccff1ca0e7", null ],
    [ "StartPosition", "classProtonEventGen.html#a2add648dc06a3a92a14b6d6e8bcb7483", null ],
    [ "TotalCrossSection", "classProtonEventGen.html#a3f6b65f44040ba2ad889761c87a36bd7", null ],
    [ "channels", "classProtonEventGen.html#a538c285bce5de9522a2522c6de9612c4", null ],
    [ "tcs", "classProtonEventGen.html#ae27cf562c22af8ac64243a3da5c2d3b1", null ]
];