var classFragmentCrossSectionFCN =
[
    [ "FragmentCrossSectionFCN", "classFragmentCrossSectionFCN.html#a54a67aa92ff807a7030ee9e63e9e03f0", null ],
    [ "~FragmentCrossSectionFCN", "classFragmentCrossSectionFCN.html#aa01b3ca7a18afebda7a0dd4a1cdc64a3", null ],
    [ "ClassDef", "classFragmentCrossSectionFCN.html#acb24a9b2cf3b2c05a5697a26ed3edba1", null ],
    [ "GenerateFrags", "classFragmentCrossSectionFCN.html#a8b9d6b536de8664d975b6ce102187207", null ],
    [ "HistoFragmentos", "classFragmentCrossSectionFCN.html#a6f3a1351369fb3d8406753c4a5d4e5ee", null ],
    [ "operator()", "classFragmentCrossSectionFCN.html#a6d8d1c0bd26034ce989c063e748fe43e", null ],
    [ "PrintParams", "classFragmentCrossSectionFCN.html#aa713d015770ff9ea996e6bb91c12d6c4", null ],
    [ "Up", "classFragmentCrossSectionFCN.html#a1f1000a4c7dd4af28f73a39d0d24acd6", null ],
    [ "UpdateParams", "classFragmentCrossSectionFCN.html#a7ed3964456a7fa33586da45e2f4a095d", null ],
    [ "csection", "classFragmentCrossSectionFCN.html#afdda9b2d940710512c3960cf107f5c61", null ],
    [ "err", "classFragmentCrossSectionFCN.html#a55d4705f18b6642537bbb2526f5221ca", null ],
    [ "inc", "classFragmentCrossSectionFCN.html#a0ef80c462d97e6b8ad40fb960e62c542", null ],
    [ "mass", "classFragmentCrossSectionFCN.html#aafb3d06089ea02a86b42cb8363304d80", null ],
    [ "mf", "classFragmentCrossSectionFCN.html#ae8ae43b8ee895c0a05721d1ec918e99b", null ],
    [ "rank", "classFragmentCrossSectionFCN.html#a6cfd95afd0afebd625b889fb6e58371c", null ],
    [ "Seed", "classFragmentCrossSectionFCN.html#a334bf4a61a9286f2196d52958ed8609b", null ],
    [ "size", "classFragmentCrossSectionFCN.html#a439227feff9d7f55384e8780cfc2eb82", null ]
];