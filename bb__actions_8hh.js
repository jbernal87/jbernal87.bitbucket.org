var bb__actions_8hh =
[
    [ "bb_elastic", "bb__actions_8hh.html#a201b0f77872d2b1c9a5ba700141696ce", null ],
    [ "bb_elastic_np", "bb__actions_8hh.html#ad3b95a01fe18a1a9010f0a68938d9f5a", null ],
    [ "bb_inelastic", "bb__actions_8hh.html#aced73181b6f64ac802d7473482a54f4f", null ],
    [ "elastic_np_angle", "bb__actions_8hh.html#afa7bd1ca176117e33bfb6df17c429cb0", null ],
    [ "NDelta_inelastic", "bb__actions_8hh.html#a7327479e31f6856b46a90808ac5f342b", null ],
    [ "NLambda_inelastic", "bb__actions_8hh.html#a011477c3e2991478d6e0b7db99c7799f", null ],
    [ "NN_inelastic", "bb__actions_8hh.html#a802c8f89229326fd0364c8ab152c947e", null ],
    [ "np_inelastic", "bb__actions_8hh.html#ac662a5c157276270614aacfb3502a3e1", null ]
];