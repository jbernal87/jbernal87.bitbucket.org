var structDataLangevinStruct =
[
    [ "DataLangevinStruct", "structDataLangevinStruct.html#a2ba38760b1bc99452f93c336242bba81", null ],
    [ "ReadData", "structDataLangevinStruct.html#af90779ab02a01bcc52036ebe73b67b45", null ],
    [ "A", "structDataLangevinStruct.html#a3c3797422e65f1fdaeb4f453a8bbe4e3", null ],
    [ "cf", "structDataLangevinStruct.html#a9d6a5a9d61948bfa3495393034cdc042", null ],
    [ "energy", "structDataLangevinStruct.html#abe9ac9fd706eb1192f668af7e3efb3cf", null ],
    [ "generator", "structDataLangevinStruct.html#a333ad1d042e4996957fc82985fa29f1f", null ],
    [ "IsDataOk", "structDataLangevinStruct.html#af7d3b52054c1a4aa9ba3869d0498385a", null ],
    [ "LangevinNumber", "structDataLangevinStruct.html#a28bbff29a804c4408dd8382b6d3d775a", null ],
    [ "mode", "structDataLangevinStruct.html#a51dda0b7f72437f75fcd4c7e01a9920c", null ],
    [ "nevents", "structDataLangevinStruct.html#a7c9b8d83b67bbf416c013248ea4d56ff", null ],
    [ "nucleusName", "structDataLangevinStruct.html#a342d0962a1b606d411ab4725576a3bf3", null ],
    [ "number_of_energies", "structDataLangevinStruct.html#a375f1e8c57bf4b337edfbf05709444a4", null ],
    [ "observable", "structDataLangevinStruct.html#a89a92f1af0bc09183578e722db92ab59", null ],
    [ "runs", "structDataLangevinStruct.html#a35ee160fe0e0f41d40d09d5ae8320ac2", null ],
    [ "step_energy", "structDataLangevinStruct.html#afa0a65583d4884123eac1abb9e5fde3f", null ],
    [ "Z", "structDataLangevinStruct.html#a893f22eaa76049acda8f7bfced87ae99", null ]
];