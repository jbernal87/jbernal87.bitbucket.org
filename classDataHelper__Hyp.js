var classDataHelper__Hyp =
[
    [ "DataHelper_Hyp", "classDataHelper__Hyp.html#ac82e3f142f5633b7b4bd7396da56ab15", null ],
    [ "~DataHelper_Hyp", "classDataHelper__Hyp.html#a4c052742d5faf76e7183cd364e38a4ef", null ],
    [ "Get_NCol", "classDataHelper__Hyp.html#aa6de4309a3eeb25f977bee863e11622c", null ],
    [ "Get_tuple_1Value", "classDataHelper__Hyp.html#a209b9e085ba43d9352cccccb4418011b", null ],
    [ "Get_tuple_2Value", "classDataHelper__Hyp.html#a88a34e5e07f51add535f8962f12ea658", null ],
    [ "Get_tuple_3Value", "classDataHelper__Hyp.html#ae46fbb6c776df8da53aba73069f74e30", null ],
    [ "GetTGraph2D", "classDataHelper__Hyp.html#a7c0b57cb846d4e8d327e53adae2b1996", null ],
    [ "GetTGraphErrorsCombN", "classDataHelper__Hyp.html#abea513a1e0e9119197b82edf2ec170d3", null ],
    [ "GetTGraphErrorsN", "classDataHelper__Hyp.html#a24539e84ff533d860fb82375c1fb4a7c", null ],
    [ "GetTNtuple", "classDataHelper__Hyp.html#ac57cee7d67f9a26a5846201c2d5c02ea", null ],
    [ "Print1_Tuple", "classDataHelper__Hyp.html#affe79b25e10e0983ad76b15bcedafcfe", null ],
    [ "PrintAll_Tuple", "classDataHelper__Hyp.html#a49f1081c73c1fcfe381b3e53e047cb88", null ],
    [ "ReadData", "classDataHelper__Hyp.html#ad5e64d9dc243f2095f67e18b44526a13", null ],
    [ "columnsLength", "classDataHelper__Hyp.html#a9409bd2334623af04849b5dff5f2e1c1", null ],
    [ "ifs", "classDataHelper__Hyp.html#a4793e3cb1414843fc07a36daee3c0852", null ],
    [ "lista", "classDataHelper__Hyp.html#ac125b70aaf08c5b0ea11a46dc704eaa8", null ],
    [ "N_Col", "classDataHelper__Hyp.html#abcf88c3da00282b5bbc799d03cd1ef96", null ],
    [ "tuple", "classDataHelper__Hyp.html#a7cecadf423f4b06382364202856a03f3", null ]
];