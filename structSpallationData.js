var structSpallationData =
[
    [ "SpallationData", "structSpallationData.html#a799e8b8266e79b83e926c33b6102829c", null ],
    [ "SpallationData", "structSpallationData.html#a5abeccff4343d12ada64c966186960fc", null ],
    [ "A", "structSpallationData.html#a56a52f73797a8d06045cf5bc1ab831bf", null ],
    [ "BarreiraFissao", "structSpallationData.html#a1b33df28a8ee83c43befc443eec7fa57", null ],
    [ "Cascades", "structSpallationData.html#a772a997a489ffbfcd5bdf1e92e54d307", null ],
    [ "DensidadeNiveis", "structSpallationData.html#a1046f1a027f7598960549b35d4b66691", null ],
    [ "Energy", "structSpallationData.html#a30227b8974f00e031efb6ba4aa8f980a", null ],
    [ "ExpData", "structSpallationData.html#a39d05993313c3de083f52751256ebc21", null ],
    [ "ExpMax", "structSpallationData.html#a71f1bdb45496a8abe30665133fc36dc4", null ],
    [ "ExpMin", "structSpallationData.html#a809f5a88acbab6ab97ae71730783567c", null ],
    [ "Graphs", "structSpallationData.html#a496e79a7e795cea14182a1838865d4e4", null ],
    [ "N", "structSpallationData.html#a7722c8ecbb62d99aee7ce68b1752f337", null ],
    [ "NucleusName", "structSpallationData.html#ae51cb650ede63c01b3a911453177b395", null ],
    [ "Results", "structSpallationData.html#acaafc27b83984767aa36c4a689f1cb8c", null ],
    [ "Z", "structSpallationData.html#a5ed5bfe6933ed8cba853237650cc041b", null ],
    [ "Z_final", "structSpallationData.html#afef6b0472414d8d88f3fb65dc2bf8afd", null ]
];