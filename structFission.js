var structFission =
[
    [ "fiss_A", "structFission.html#a67af68837450e82dfcb905d0f3952f98", null ],
    [ "fiss_alpha", "structFission.html#a8795dc4b9535f14f1f606f08368ac5d4", null ],
    [ "fiss_E", "structFission.html#a0bb289c05bb10090b4cffd11ac6a64c9", null ],
    [ "fiss_neutron", "structFission.html#ac74fd275c34ef046a55031560d7b0e0a", null ],
    [ "fiss_proton", "structFission.html#ac9516aa56b50375261d1ff5e19c24883", null ],
    [ "fiss_Z", "structFission.html#a67cc9a81e2b8abac6eadc50596a723ae", null ],
    [ "fissility", "structFission.html#af9df8f7b4e966f0b5fa17b1e94bd9210", null ]
];