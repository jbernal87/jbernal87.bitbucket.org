var classPhotonEventGen =
[
    [ "PhotonEventGen", "classPhotonEventGen.html#a671571023b4827be477a02e729c91692", null ],
    [ "~PhotonEventGen", "classPhotonEventGen.html#aa3a5471f94bfcac8ff5d096f3545425e", null ],
    [ "ClassDef", "classPhotonEventGen.html#ad3b3da0c5ca12fe58888772c27c89f0c", null ],
    [ "Generate", "classPhotonEventGen.html#aac201211488c320c072777401de33343", null ],
    [ "Generate", "classPhotonEventGen.html#a718e07ada1fc4f8404f5665069dbeef2", null ],
    [ "Generate", "classPhotonEventGen.html#a85e5088eb8507d0dd711e5efd4912799", null ],
    [ "GetChannels", "classPhotonEventGen.html#a417c98e795c3acfa89854cccff1ca0e7", null ],
    [ "NumPhotons", "classPhotonEventGen.html#a3884f1ff7e849dfe6031cb36bd8c0d2b", null ],
    [ "ResetNumPhotons", "classPhotonEventGen.html#afd650f6255c28d2afe19bcacfdf6839b", null ],
    [ "SelectNucleons", "classPhotonEventGen.html#a3f5e153e8a6a0ff06f210093d28a6c77", null ],
    [ "SetVirtuality", "classPhotonEventGen.html#af49e2ad72107a89eb293d5cb0fff4466", null ],
    [ "StartPosition", "classPhotonEventGen.html#a2add648dc06a3a92a14b6d6e8bcb7483", null ],
    [ "TotalCrossSection", "classPhotonEventGen.html#afabcb3afc94ed8ad374289df0ace6bba", null ],
    [ "channels", "classPhotonEventGen.html#a538c285bce5de9522a2522c6de9612c4", null ],
    [ "num_photons", "classPhotonEventGen.html#a83b5e285fd750cf0c3c2c30b2d370c55", null ],
    [ "Q_2", "classPhotonEventGen.html#a83431d0264c26c6c2fd82131758ae5be", null ],
    [ "selected_ch", "classPhotonEventGen.html#a942d30c5c6ae73bda2b89e1ab97fd97a", null ],
    [ "selected_nucleon", "classPhotonEventGen.html#a6ea68b9c838b24e9e8258154e84f9ce5", null ],
    [ "tcs", "classPhotonEventGen.html#ae27cf562c22af8ac64243a3da5c2d3b1", null ]
];