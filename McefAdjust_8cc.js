var McefAdjust_8cc =
[
    [ "kLINEAR", "McefAdjust_8cc.html#a93149a2165eef243f0b0c78de6ab3560", null ],
    [ "LoadSpallationData", "McefAdjust_8cc.html#a32ea8c8e1843fed97c2f054ef2462788", null ],
    [ "PhotoFission", "McefAdjust_8cc.html#a2c9d59965a7e9734e3c1d1d70d467c02", null ],
    [ "SpallationCS_MPI", "McefAdjust_8cc.html#a16b68ebaa498d92446e5c69c9d18e18b", null ],
    [ "__MAX_A", "McefAdjust_8cc.html#acb1f9aa0f968d3cf838729a066a860c0", null ],
    [ "__MAX_Z", "McefAdjust_8cc.html#a1717a019861a95c7596bd39a5ae0c69c", null ],
    [ "extraSpallation", "McefAdjust_8cc.html#a0e7dd53cc0bc89c8688884d30bef08a9", null ],
    [ "frequencias", "McefAdjust_8cc.html#ad5bbefdfaed24083b5d4190c43161222", null ],
    [ "local_frequencias", "McefAdjust_8cc.html#a7132e62c1204e5f87116fe3254872051", null ],
    [ "McefModel_os", "McefAdjust_8cc.html#a92bdf6586bf394ce90173bd68df20ed3", null ],
    [ "X", "McefAdjust_8cc.html#a00e3d0f373aad295634d5bcfe761d1f3", null ]
];