var classAnalysisUPC =
[
    [ "AnalysisUPC", "classAnalysisUPC.html#a21aac5dadcf6c76f9c5338b9b30ad54e", null ],
    [ "~AnalysisUPC", "classAnalysisUPC.html#add50b57f867992a6d28ab4bcb97ea644", null ],
    [ "ComputeCrossSection", "classAnalysisUPC.html#a1bb8aadf7b540b57aadf2dc194227fd8", null ],
    [ "ConstructPhotonFlux", "classAnalysisUPC.html#a58f720337fdab4aa54cb80d37789e7f4", null ],
    [ "Get_dsigmadE", "classAnalysisUPC.html#a8a99a1d1f61b5ad100e9c991e5b95f1b", null ],
    [ "GetBackRapidityHistVector", "classAnalysisUPC.html#a8c40d8d641e38d654d24aa2d1073ce1b", null ],
    [ "GetCSBack", "classAnalysisUPC.html#af7b938fdd5d7b1f929ea9a26a883202f", null ],
    [ "GetCSForw", "classAnalysisUPC.html#a8acdd5bdf2a217e47f9b1b65c567c6df", null ],
    [ "GetCSTotal", "classAnalysisUPC.html#a5e1421de33658da43189f64cba2493bb", null ],
    [ "GetForwRapidityHistVector", "classAnalysisUPC.html#ad542b38c65f8b6da041c04d365846414", null ],
    [ "GetGeomCS", "classAnalysisUPC.html#ae41c596ad2f29353909ce3b4897a5a6a", null ],
    [ "GetProjRadius", "classAnalysisUPC.html#a27aa29a0675c81dfef25034e0b3c0f21", null ],
    [ "GetpTHistVector", "classAnalysisUPC.html#a1f9f60f4d8c985035a1ae7638b4e8e80", null ],
    [ "GetTargetRadius", "classAnalysisUPC.html#aa3ca13e5538acef5e7f8f9a7607542f1", null ],
    [ "InitHists", "classAnalysisUPC.html#a5eef63ef66386418d74fe3f13a7b3086", null ],
    [ "PhotoNuclearCS", "classAnalysisUPC.html#af04eaea899b42cb24a02bf9b78965c0d", null ],
    [ "SetA", "classAnalysisUPC.html#a0f37f99ec870356f69c66045fdcf3cf7", null ],
    [ "SetAProj", "classAnalysisUPC.html#a7cdabedcee84d6644b37f91cd7a5783a", null ],
    [ "SetATarget", "classAnalysisUPC.html#a38c67e32920dfa686d22c03b355b109c", null ],
    [ "SetCMEnergy", "classAnalysisUPC.html#ababc80df19e67aff7a82a67d53212c05", null ],
    [ "SetCSUnit", "classAnalysisUPC.html#a24f2f7846d426df9c4a9bf23dc415b91", null ],
    [ "SetGammaProj", "classAnalysisUPC.html#af720b5fec5940fbe1786371ea06e0e4f", null ],
    [ "SetmpiProcN", "classAnalysisUPC.html#a6796c8f588cec6025f62cf8fea5b794a", null ],
    [ "SetNEnergies", "classAnalysisUPC.html#ab2e5ed1cc5419d4ed8996bfedd4b97c0", null ],
    [ "SetNevents", "classAnalysisUPC.html#a8184fc9ff85314e42f202eb3d3fff52b", null ],
    [ "SetNucleusName", "classAnalysisUPC.html#a6e70bab976ef71073acb4f3aa8e1a7a1", null ],
    [ "SetProcess", "classAnalysisUPC.html#abccca4d16302c274d329046b16d82e8f", null ],
    [ "SetProjName", "classAnalysisUPC.html#a695c8d090dffe3aef2ab25eae0fb57a5", null ],
    [ "SetTargetName", "classAnalysisUPC.html#ab185eddcc9b0244234448d9c26fb4f2e", null ],
    [ "SetVarRedFactor", "classAnalysisUPC.html#a46661025ae68f0982ec220db87bb9143", null ],
    [ "SetVMid", "classAnalysisUPC.html#a0bbb0e36d3d1ca85252d02a031ea72a7", null ],
    [ "SetVMmass", "classAnalysisUPC.html#abafe74cc67c49b4f85906f47d693add9", null ],
    [ "SetZ", "classAnalysisUPC.html#a26a38721799dfbeba20baf00348095a3", null ],
    [ "SetZProj", "classAnalysisUPC.html#a83efe75472c3ee0685e1a2e7480e8905", null ],
    [ "SetZTarget", "classAnalysisUPC.html#adf599b4f084f5aa860e9b0252cd5c0ed", null ],
    [ "A", "classAnalysisUPC.html#a56a52f73797a8d06045cf5bc1ab831bf", null ],
    [ "A_Proj", "classAnalysisUPC.html#ae0211e9cd363935a17aad12c43b2a3fc", null ],
    [ "A_Target", "classAnalysisUPC.html#a2cce6f447b6ea5d3d3ac51a7af15623f", null ],
    [ "CMEnergy", "classAnalysisUPC.html#aba2bb3043957f98f476c6395feaa1225", null ],
    [ "CS", "classAnalysisUPC.html#a67e63547f68fe82c3c23e83397dcafae", null ],
    [ "CSYBack", "classAnalysisUPC.html#a1a1393e080976e4c794ec0c983380371", null ],
    [ "CSYForw", "classAnalysisUPC.html#ad798844630fb68affaf9fc885a2b239b", null ],
    [ "CSYTotal", "classAnalysisUPC.html#ad6a7529bfb70247beaf3b5a87adcad1e", null ],
    [ "GammaBeam", "classAnalysisUPC.html#ac29b647ee7f2becc12b8506ee68ac9de", null ],
    [ "GammaProj", "classAnalysisUPC.html#acfa4cd1954cb3d9b61b2e8b401a10faa", null ],
    [ "mpiProcN", "classAnalysisUPC.html#a6184871014facbb6c70b41174aa4a0d4", null ],
    [ "Nenergies", "classAnalysisUPC.html#ad1c497edf9c2e223e087c180513b08f0", null ],
    [ "Nevents", "classAnalysisUPC.html#ab8ca9d7503f2bbf376d173b4b7cf6771", null ],
    [ "NucleusName", "classAnalysisUPC.html#ab5ab062517a1057547a08dbba3c2e2c8", null ],
    [ "ProjName", "classAnalysisUPC.html#a2f32c67dba970a8d127eed9a31b7d97a", null ],
    [ "pTHistEn", "classAnalysisUPC.html#a7c867c80e9c1d30f91a2d014997a25b4", null ],
    [ "Rapidity", "classAnalysisUPC.html#aca42926afb36a52d9a20d1795d3353f3", null ],
    [ "RapidityEnBack", "classAnalysisUPC.html#ace083897ed48a6ff92c361ecf60a87a0", null ],
    [ "RapidityEnForw", "classAnalysisUPC.html#a30a04bdb3f5e7a457fd4c51eeedb819b", null ],
    [ "TargetName", "classAnalysisUPC.html#a79621a23fbb8379c7e54fff08376410b", null ],
    [ "unit", "classAnalysisUPC.html#a4a04b9b6e9dddf9d11bbf307b202f95d", null ],
    [ "VarRedFactor", "classAnalysisUPC.html#ac06c61aeeccb52301720b68abe2793d8", null ],
    [ "VMid", "classAnalysisUPC.html#a3e91d55cd83417c99b71751a878e1d07", null ],
    [ "VMmass", "classAnalysisUPC.html#a250a3aa59760f97d369c54e7f76f5609", null ],
    [ "VMName", "classAnalysisUPC.html#a8aaa17eed3305bd4941a49d27d0a76cd", null ],
    [ "Z", "classAnalysisUPC.html#a5ed5bfe6933ed8cba853237650cc041b", null ],
    [ "Z_Proj", "classAnalysisUPC.html#a8dabb171219cc47ad20451f02066b7f4", null ],
    [ "Z_Target", "classAnalysisUPC.html#a6cda9d7bf01b9660cb2c7272fa7275b5", null ]
];