var structDataStruct =
[
    [ "DataStruct", "structDataStruct.html#ab98041938bc538e53059ac9778fc958c", null ],
    [ "ReadData", "structDataStruct.html#af90779ab02a01bcc52036ebe73b67b45", null ],
    [ "A", "structDataStruct.html#a3c3797422e65f1fdaeb4f453a8bbe4e3", null ],
    [ "CascNumber", "structDataStruct.html#a5c34f0c4f4a2fc7b76f2ca229489fd03", null ],
    [ "IsDataOk", "structDataStruct.html#af7d3b52054c1a4aa9ba3869d0498385a", null ],
    [ "nucleusName", "structDataStruct.html#a342d0962a1b606d411ab4725576a3bf3", null ],
    [ "number_of_energies", "structDataStruct.html#a375f1e8c57bf4b337edfbf05709444a4", null ],
    [ "number_of_times_run", "structDataStruct.html#a9ac2ba74b3deae7da6e8e56d0a502934", null ],
    [ "Parameters", "structDataStruct.html#a6c22270f87d6a7f17c3cb674379dd9ed", null ],
    [ "Print_Internal_Casc", "structDataStruct.html#acb69a944ef5c3308667cf567d5ac6aa9", null ],
    [ "printUnbindParticles", "structDataStruct.html#a17692ccb667074f42b9ce38987313c18", null ],
    [ "seed", "structDataStruct.html#a22b82ee6d8c13f64a569b81035b54947", null ],
    [ "start_energy", "structDataStruct.html#a4aba261e7898a7a6731a2d5420465aea", null ],
    [ "step_energy", "structDataStruct.html#add549c512bf8d9b3db3ff8dda8d11bd8", null ],
    [ "TyCas", "structDataStruct.html#aff5dc6ff8d38148a28c5f2e5eb85525c", null ],
    [ "VectPar", "structDataStruct.html#a81eea1b44a05af8e343c0e0cac505529", null ],
    [ "Z", "structDataStruct.html#a893f22eaa76049acda8f7bfced87ae99", null ]
];