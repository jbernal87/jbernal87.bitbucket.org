var structDataMcefStruct =
[
    [ "DataMcefStruct", "structDataMcefStruct.html#a3dea4a502ae08f3b02cf7c6fa98f8a5b", null ],
    [ "ReadData", "structDataMcefStruct.html#af90779ab02a01bcc52036ebe73b67b45", null ],
    [ "A", "structDataMcefStruct.html#a3c3797422e65f1fdaeb4f453a8bbe4e3", null ],
    [ "energy", "structDataMcefStruct.html#abe9ac9fd706eb1192f668af7e3efb3cf", null ],
    [ "generator", "structDataMcefStruct.html#a333ad1d042e4996957fc82985fa29f1f", null ],
    [ "IsDataOk", "structDataMcefStruct.html#af7d3b52054c1a4aa9ba3869d0498385a", null ],
    [ "McefNumber", "structDataMcefStruct.html#a709243fc1c279bd546bc88d4dc798ba2", null ],
    [ "modelBarrier", "structDataMcefStruct.html#afff01b5475ae1f263bd1b110677db580", null ],
    [ "modelLevelDens", "structDataMcefStruct.html#acbf79bb5cace91cb1fa095bb2ac6d5f9", null ],
    [ "nevents", "structDataMcefStruct.html#a7c9b8d83b67bbf416c013248ea4d56ff", null ],
    [ "nucleusName", "structDataMcefStruct.html#a342d0962a1b606d411ab4725576a3bf3", null ],
    [ "number_of_energies", "structDataMcefStruct.html#a375f1e8c57bf4b337edfbf05709444a4", null ],
    [ "runs", "structDataMcefStruct.html#a35ee160fe0e0f41d40d09d5ae8320ac2", null ],
    [ "step_energy", "structDataMcefStruct.html#afa0a65583d4884123eac1abb9e5fde3f", null ],
    [ "WeissEnergyDist", "structDataMcefStruct.html#a446422c18d176b30f20cfb590ae1d405", null ],
    [ "WeissWidth", "structDataMcefStruct.html#a1430399fd1d4c6c06be0887bde1c0839", null ],
    [ "Z", "structDataMcefStruct.html#a893f22eaa76049acda8f7bfced87ae99", null ]
];