var classMeasurement =
[
    [ "Measurement", "classMeasurement.html#ac79b0662bb46d4ce9a480556c7456b68", null ],
    [ "Measurement", "classMeasurement.html#a7e89ccc7b2fa23601450df34009062fe", null ],
    [ "Measurement", "classMeasurement.html#aebeacfd8a09e9151eba64705e01be067", null ],
    [ "Measurement", "classMeasurement.html#a867811c3eaf7e91eb439417d06bcc6d2", null ],
    [ "~Measurement", "classMeasurement.html#a2ed11b7dabd4e2c204b8645dace27f7c", null ],
    [ "Ch_sigma", "classMeasurement.html#aa154a6befdbf62d29ae05b960e7aa3bf", null ],
    [ "Ch_value", "classMeasurement.html#aa1e767de358eaa99b95464de6b5315ae", null ],
    [ "ClassDef", "classMeasurement.html#a1897c71889005ade91e3104fdd653510", null ],
    [ "error", "classMeasurement.html#ac726bfdbc010d3ac48e2e3093b408fd9", null ],
    [ "operator<", "classMeasurement.html#a316d152b9ab4a69c076753aee38a5430", null ],
    [ "operator=", "classMeasurement.html#a0bacdceae165489ef2aed3a9ca8200d1", null ],
    [ "operator==", "classMeasurement.html#a58156aa3113e484144ec1003fb57e450", null ],
    [ "sigma", "classMeasurement.html#a02608694d0a74cf0983ae639bb3c66f9", null ],
    [ "swap", "classMeasurement.html#a3129fead7224c7fb5375dcf2fefcbc0b", null ],
    [ "value", "classMeasurement.html#ac25a1dff53df1bd7dc9f0844ee04ec82", null ],
    [ "chann_sig", "classMeasurement.html#a74b23f396ff67d3f128e8b6215a1414c", null ],
    [ "chann_val", "classMeasurement.html#a41932fb01e5a215254d275e91c99f55b", null ],
    [ "err", "classMeasurement.html#a55d4705f18b6642537bbb2526f5221ca", null ],
    [ "sig", "classMeasurement.html#a8fea7c23e50be8a1bc4091cc6515105f", null ],
    [ "val", "classMeasurement.html#a3c8bc0bb4f045bfe78d1196cb786792c", null ]
];