var searchData=
[
  ['kaon_5felastic_5fcross_5fsection',['kaon_elastic_cross_section',['../kaon__n__cross__sections_8cc.html#a8f095f1ceeb1e3bc5c9fa47007e7d872',1,'kaon_elastic_cross_section(Dynamics *kaon, Dynamics *nucleon, Double_t *cch):&#160;kaon_n_cross_sections.cc'],['../kaon__n__cross__sections_8hh.html#a8f095f1ceeb1e3bc5c9fa47007e7d872',1,'kaon_elastic_cross_section(Dynamics *kaon, Dynamics *nucleon, Double_t *cch):&#160;kaon_n_cross_sections.cc']]],
  ['kinetic_5fenergy',['kinetic_energy',['../classdynamic.html#a62f1110623460569c519cce454328cfb',1,'dynamic::kinetic_energy(double Ex_max, double aj, int A, bool neutron=false)'],['../classdynamic.html#a62f1110623460569c519cce454328cfb',1,'dynamic::kinetic_energy(double Ex_max, double aj, int A, bool neutron=false)'],['../classdynamic.html#a62f1110623460569c519cce454328cfb',1,'dynamic::kinetic_energy(double Ex_max, double aj, int A, bool neutron=false)']]]
];
