var structFission__l =
[
    [ "fiss_A", "structFission__l.html#a67af68837450e82dfcb905d0f3952f98", null ],
    [ "fiss_alpha", "structFission__l.html#a8795dc4b9535f14f1f606f08368ac5d4", null ],
    [ "fiss_E", "structFission__l.html#a0bb289c05bb10090b4cffd11ac6a64c9", null ],
    [ "fiss_l", "structFission__l.html#a67675c0d4306db0392db73fc2d831192", null ],
    [ "fiss_neutron", "structFission__l.html#ac74fd275c34ef046a55031560d7b0e0a", null ],
    [ "fiss_proton", "structFission__l.html#ac9516aa56b50375261d1ff5e19c24883", null ],
    [ "fiss_q", "structFission__l.html#a6fb4606a690ad62a72bac6e92474ee07", null ],
    [ "fiss_qes", "structFission__l.html#a1dfc8b5f0cc9f1dfb2e92d5c05266499", null ],
    [ "fiss_Z", "structFission__l.html#a67cc9a81e2b8abac6eadc50596a723ae", null ],
    [ "fissility", "structFission__l.html#af9df8f7b4e966f0b5fa17b1e94bd9210", null ]
];