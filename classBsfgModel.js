var classBsfgModel =
[
    [ "BsfgModel", "classBsfgModel.html#ac46c6fa512f90b1cd99f56b5735d2fa5", null ],
    [ "BsfgModel", "classBsfgModel.html#a9b29aed3db826ce77536d2dee910e490", null ],
    [ "a_modelo_BSFG", "classBsfgModel.html#a57af6b8dde04a6d89b4318daf0d6bf1b", null ],
    [ "CalculateAa", "classBsfgModel.html#ad31ebf2cf05a0b98d306cd042b2c63dd", null ],
    [ "CalculateAn", "classBsfgModel.html#a6a6899f9ad3d27f8c838bf7f07da1721", null ],
    [ "CalculateAp", "classBsfgModel.html#a1a79dfcb54c78fe7711e5f8bbc7ad5c2", null ],
    [ "CalculateMass", "classBsfgModel.html#ab6bcf3b37fc229f4a9e4fcd40b3c11ca", null ],
    [ "CalculatePd", "classBsfgModel.html#ad38673e262e276d8f9de633239db8631", null ],
    [ "CalculateSd", "classBsfgModel.html#ae9a3fe47cfdb6386a69482745e18819b", null ],
    [ "ClassDef", "classBsfgModel.html#ac71949d74cd9c70a330a6946d33e6640", null ],
    [ "CorrectionDerived", "classBsfgModel.html#aa106491cc263b2c81dcc0afe5c9e8e10", null ],
    [ "MassCorrection", "classBsfgModel.html#a182424a097cd1780d78af7b3c8234478", null ],
    [ "Mexp", "classBsfgModel.html#a228898499ffb30d5e04fa6a195a8013a", null ]
];