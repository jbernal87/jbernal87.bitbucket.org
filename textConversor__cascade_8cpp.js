var textConversor__cascade_8cpp =
[
    [ "if", "textConversor__cascade_8cpp.html#a5ce66791e28446d0eb907a6e28631636", null ],
    [ "f", "textConversor__cascade_8cpp.html#a633de4b0c14ca52ea2432a3c8a5c4c31", null ],
    [ "t2", "textConversor__cascade_8cpp.html#a473502fcfa9c404ccbd6fab6e07eb9d4", null ],
    [ "TBNuc_A", "textConversor__cascade_8cpp.html#a28239b660fb1b40c425fa4b933472495", null ],
    [ "TBNuc_Energy", "textConversor__cascade_8cpp.html#a0429094014b5404cfe32783a5076c0eb", null ],
    [ "TBNuc_phoCnt", "textConversor__cascade_8cpp.html#aa23d695d7f43929942d6db7c1d3a3f19", null ],
    [ "TBNuc_Z", "textConversor__cascade_8cpp.html#ae47ce155d8ffb14c42a2694a1a769eee", null ],
    [ "TBPart_E", "textConversor__cascade_8cpp.html#ade175777a75319b9d3b23dcd3bc9ba58", null ],
    [ "TBPart_K", "textConversor__cascade_8cpp.html#a3bdfdc36cadd07429d9101b3451cf3eb", null ],
    [ "TBPart_Mes_PID", "textConversor__cascade_8cpp.html#a8a4dd8d533f2b4f93231e15cdf867b40", null ],
    [ "TBPart_NPart", "textConversor__cascade_8cpp.html#ae289b7e906a97e82b74be8c75b720bf0", null ],
    [ "TBPart_PX", "textConversor__cascade_8cpp.html#aa4339809283e2068f7b435307466cfc8", null ],
    [ "TBPart_PY", "textConversor__cascade_8cpp.html#a1113ac87b723ddd003ceddead68c7a59", null ],
    [ "TBPart_PZ", "textConversor__cascade_8cpp.html#a8d2009b92383b87ba819e793a93c1d63", null ]
];