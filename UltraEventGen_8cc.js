var UltraEventGen_8cc =
[
    [ "ClassImp", "UltraEventGen_8cc.html#a8eb50390e015c435a77848eecb37e01c", null ],
    [ "integrand", "UltraEventGen_8cc.html#a8da95a0d5b36101573541844f1821c88", null ],
    [ "PhotonFluxFunction", "UltraEventGen_8cc.html#aecccb2d5dc87eec3620488ee5ac2ab52", null ],
    [ "spectro", "UltraEventGen_8cc.html#aba088958234c1e329dacee8e3e11133b", null ],
    [ "survival_probability", "UltraEventGen_8cc.html#a1f7816ffc2b04352055429fbbf39b6ae", null ],
    [ "alpha", "UltraEventGen_8cc.html#a15413e11c48afbe5bad98d8e8b406a30", null ],
    [ "c", "UltraEventGen_8cc.html#a5af07319d7427c6711be099ffba5cc92", null ],
    [ "e", "UltraEventGen_8cc.html#a7958f4791acb042ba41abd6eb01f3955", null ],
    [ "hbar", "UltraEventGen_8cc.html#a2a7f9197f7433e1d78a95058fceaa4c8", null ]
];