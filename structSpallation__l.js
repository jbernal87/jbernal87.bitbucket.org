var structSpallation__l =
[
    [ "l", "structSpallation__l.html#a14b4499bb0daea8fdd45eda8bb28011f", null ],
    [ "spall_A", "structSpallation__l.html#a34c46f165d0da7058641183bd7a4ab07", null ],
    [ "spall_alpha", "structSpallation__l.html#a3275ba77d7f67f345259a423cc530086", null ],
    [ "spall_neutron", "structSpallation__l.html#a8f2f2912c495b996eac99840ae439776", null ],
    [ "spall_proton", "structSpallation__l.html#a8ebd891a96ef8d8dc932b072af532361", null ],
    [ "spall_q", "structSpallation__l.html#a3f0758f019e2e423a5b2578cff286d95", null ],
    [ "spall_qes", "structSpallation__l.html#a5c2a68f784cd0baacddb0abdecd41de9", null ],
    [ "spall_Z", "structSpallation__l.html#ac6a09a1e75737f0267c08a13fc76acc7", null ]
];