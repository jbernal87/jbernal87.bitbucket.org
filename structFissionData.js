var structFissionData =
[
    [ "FissionData", "structFissionData.html#a2ef183e98f5b6fd23072278c7eb9e68e", null ],
    [ "FissionData", "structFissionData.html#a8dc0395e5c71c49cd78b013ac0c43db3", null ],
    [ "A", "structFissionData.html#a56a52f73797a8d06045cf5bc1ab831bf", null ],
    [ "BarreiraFissao", "structFissionData.html#a1b33df28a8ee83c43befc443eec7fa57", null ],
    [ "Cascades", "structFissionData.html#a772a997a489ffbfcd5bdf1e92e54d307", null ],
    [ "DensidadeNiveis", "structFissionData.html#a1046f1a027f7598960549b35d4b66691", null ],
    [ "Energies", "structFissionData.html#abc6619f669cb343c95207413d7596f5d", null ],
    [ "ExpData", "structFissionData.html#a39d05993313c3de083f52751256ebc21", null ],
    [ "ExpMax", "structFissionData.html#a71f1bdb45496a8abe30665133fc36dc4", null ],
    [ "ExpMin", "structFissionData.html#a809f5a88acbab6ab97ae71730783567c", null ],
    [ "Graphs", "structFissionData.html#a496e79a7e795cea14182a1838865d4e4", null ],
    [ "NucleusName", "structFissionData.html#ae51cb650ede63c01b3a911453177b395", null ],
    [ "Results", "structFissionData.html#acaafc27b83984767aa36c4a689f1cb8c", null ],
    [ "Z", "structFissionData.html#a5ed5bfe6933ed8cba853237650cc041b", null ]
];