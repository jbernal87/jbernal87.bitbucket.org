var structNuc__Struct =
[
    [ "CPT_Print", "structNuc__Struct.html#ae473b77529066a098700d3eafa0088ec", null ],
    [ "Print", "structNuc__Struct.html#a9dcac18006ce057b8d78c847174c1362", null ],
    [ "Print_Levels", "structNuc__Struct.html#ae9851197e51a62d3245d1cec9d74ab03", null ],
    [ "A", "structNuc__Struct.html#a8173cbeb15f6f880d64cbbf892cdf9ea", null ],
    [ "NLevels", "structNuc__Struct.html#aad1bcea535eab976502dfb4e58c8df33", null ],
    [ "Nuc_E", "structNuc__Struct.html#a8e145e033b65ce7f852137e984a0f5b0", null ],
    [ "Nuc_IDX", "structNuc__Struct.html#a0e6f8d7fd450baf36f3ed6360d3d506b", null ],
    [ "Nuc_IsB", "structNuc__Struct.html#a70b333495bbaaf765bdc917b17c1b27f", null ],
    [ "Nuc_K", "structNuc__Struct.html#a667c092f2c9737a002f8f1391d89cd8c", null ],
    [ "Nuc_P", "structNuc__Struct.html#a2f2d3121375d47f557e683b56dfca38c", null ],
    [ "Nuc_PID", "structNuc__Struct.html#ae456a43ccf04886f53d9a6166dcb2d96", null ],
    [ "Nuc_PX", "structNuc__Struct.html#a763477678ada20d1072e0d78bf3c98e0", null ],
    [ "Nuc_PY", "structNuc__Struct.html#ab2ae38d87ead40716a2993b0ddb24dca", null ],
    [ "Nuc_PZ", "structNuc__Struct.html#af1652e1290cdf53a3b779fb5d9cd9513", null ],
    [ "Nuc_X", "structNuc__Struct.html#afbf645f1fa9e8578509bbf1c3ac6f906", null ],
    [ "Nuc_Y", "structNuc__Struct.html#a9c2630e5a4071019328f12f753f67271", null ],
    [ "Nuc_Z", "structNuc__Struct.html#a845517f2a1a45bac88d8066f4d8ee372", null ],
    [ "PLevels", "structNuc__Struct.html#a15297f00370f40a3c003fb13bc04cd50", null ],
    [ "Z", "structNuc__Struct.html#a782340e259182f30807b6525ef34910c", null ]
];