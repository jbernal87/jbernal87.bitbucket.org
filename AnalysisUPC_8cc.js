var AnalysisUPC_8cc =
[
    [ "alpha", "AnalysisUPC_8cc.html#aa816ab3cd347f9fb8805f6296052c9c3", null ],
    [ "c", "AnalysisUPC_8cc.html#aafc737ea9ef91f59cf9acd287fb8d085", null ],
    [ "e", "AnalysisUPC_8cc.html#a270b1ecf56619755ce62f6970dfabc91", null ],
    [ "hbar", "AnalysisUPC_8cc.html#a981ce2ae0a64e88b0a00af5d781071ad", null ],
    [ "mp", "AnalysisUPC_8cc.html#aea466a7139309b17bce61d5ccb03e195", null ],
    [ "r0", "AnalysisUPC_8cc.html#af8ef4632fb5325a43f9c31778135d450", null ],
    [ "AnalysisUPC", "AnalysisUPC_8cc.html#a0f37d595e62045ae4e98b33649433b20", null ],
    [ "analyticalN", "AnalysisUPC_8cc.html#a202de8dd2f792117ffb3bca6ec5747eb", null ],
    [ "N", "AnalysisUPC_8cc.html#a79157195b0a5a5155d248e829a583352", null ],
    [ "PhotonFluxFunction", "AnalysisUPC_8cc.html#a59f8b76829cd28fcb684a4887279902e", null ],
    [ "collision", "AnalysisUPC_8cc.html#a793a62c716a2289b03ffc0e5076fcef5", null ],
    [ "gT", "AnalysisUPC_8cc.html#a9be4b846dc917f0dac0555cd5eaa4536", null ],
    [ "survFile", "AnalysisUPC_8cc.html#a454562ae7b7caf25a70bdc2533800049", null ]
];