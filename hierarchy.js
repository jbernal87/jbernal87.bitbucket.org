var hierarchy =
[
    [ "AbstractCascadeProcess", "classAbstractCascadeProcess.html", [
      [ "BaryonBaryon", "classBaryonBaryon.html", null ],
      [ "BaryonDecay", "classBaryonDecay.html", null ],
      [ "BaryonLepton", "classBaryonLepton.html", null ],
      [ "BaryonLepton", "classBaryonLepton.html", null ],
      [ "BaryonMeson", "classBaryonMeson.html", null ],
      [ "BaryonSurface", "classBaryonSurface.html", null ],
      [ "MesonDecay", "classMesonDecay.html", null ],
      [ "MesonSurface", "classMesonSurface.html", null ]
    ] ],
    [ "AbstractChannel", "classAbstractChannel.html", [
      [ "LeptonNucleonChannel", "classLeptonNucleonChannel.html", null ],
      [ "MesonNucleonChannel", "classMesonNucleonChannel.html", null ]
    ] ],
    [ "AnalysisUPC", "classAnalysisUPC.html", null ],
    [ "BasicException", "classBasicException.html", null ],
    [ "BBChannel", "classBBChannel.html", null ],
    [ "BindingEnergyTable", "classBindingEnergyTable.html", null ],
    [ "Cascade", "classCascade.html", null ],
    [ "CascadeData", "structCascadeData.html", null ],
    [ "CascadeFileBuffer", "structCascadeFileBuffer.html", null ],
    [ "CRISP_XML_Parser", "classCRISP__XML__Parser.html", null ],
    [ "CrispParticleTable", "classCrispParticleTable.html", null ],
    [ "CrossSectionChannel", "classCrossSectionChannel.html", null ],
    [ "CrossSectionData", "classCrossSectionData.html", null ],
    [ "DataHelper_Hyp", "classDataHelper__Hyp.html", null ],
    [ "DataHelper_struct", "structDataHelper__struct.html", null ],
    [ "DataLangevinStruct", "structDataLangevinStruct.html", null ],
    [ "DataMcefStruct", "structDataMcefStruct.html", null ],
    [ "DataMultimodalFissionStruct", "structDataMultimodalFissionStruct.html", null ],
    [ "DataMultimodalFissionStructMINUIT", "structDataMultimodalFissionStructMINUIT.html", null ],
    [ "DataStruct", "structDataStruct.html", null ],
    [ "dynamic", "classdynamic.html", null ],
    [ "Dynamics", "classDynamics.html", [
      [ "NucleusDynamics", "classNucleusDynamics.html", null ],
      [ "ParticleDynamics", "classParticleDynamics.html", null ]
    ] ],
    [ "elements_rootFiles", "scripts_2langevin_2work__rootfiles_8hh.html#structelements__rootFiles", null ],
    [ "Evap_part", "structEvap__part.html", null ],
    [ "Evap_part_l", "structEvap__part__l.html", null ],
    [ "EventGen", "classEventGen.html", [
      [ "BremsstrahlungEventGen", "classBremsstrahlungEventGen.html", null ],
      [ "DeuteronEventGen", "classDeuteronEventGen.html", null ],
      [ "HyperonEventGen", "classHyperonEventGen.html", null ],
      [ "IonEventGen", "classIonEventGen.html", null ],
      [ "NeutrinoEventGen", "classNeutrinoEventGen.html", null ],
      [ "PhotonEventGen", "classPhotonEventGen.html", null ],
      [ "ProtonEventGen", "classProtonEventGen.html", null ],
      [ "UltraEventGen", "classUltraEventGen.html", null ]
    ] ],
    [ "FermiLevels", "classFermiLevels.html", null ],
    [ "Fissility", "classFissility.html", null ],
    [ "Fission", "scripts_2langevin_2hh_2HelperTemp_8hh.html#structFission", null ],
    [ "Fission_l", "DataHelper_8hh.html#structFission__l", null ],
    [ "FissionBarrier", "classFissionBarrier.html", null ],
    [ "FissionData", "structFissionData.html", null ],
    [ "FragmentCrossSectionFCN", "classFragmentCrossSectionFCN.html", null ],
    [ "function_struct", "structfunction__struct.html", null ],
    [ "Lep_Struct", "structLep__Struct.html", null ],
    [ "LeptonsPool", "classLeptonsPool.html", null ],
    [ "Mcef", "classMcef.html", null ],
    [ "McefAdjust", "classMcefAdjust.html", null ],
    [ "MCEFL", "structMCEFL.html", null ],
    [ "McefModel", "classMcefModel.html", [
      [ "BsfgModel", "classBsfgModel.html", null ]
    ] ],
    [ "Measurement", "classMeasurement.html", null ],
    [ "media_desviop", "classmedia__desviop.html", null ],
    [ "Mes_Struct", "structMes__Struct.html", null ],
    [ "MesonsPool", "classMesonsPool.html", null ],
    [ "MultimodalFission", "classMultimodalFission.html", null ],
    [ "MyFunctionObject", "classMyFunctionObject.html", null ],
    [ "NameTable", "classNameTable.html", null ],
    [ "Nuc_Struct", "structNuc__Struct.html", null ],
    [ "NuclearPotentialStruc", "structNuclearPotentialStruc.html", null ],
    [ "Nucleus", "classNucleus.html", null ],
    [ "nucleus", "structnucleus.html", null ],
    [ "PamClass", "classPamClass.html", null ],
    [ "param_rootFiles", "scripts_2langevin_2work__rootfiles_8hh.html#structparam__rootFiles", null ],
    [ "paramFunctionPot", "Langevin_2potential_8hh.html#structparamFunctionPot", null ],
    [ "paramFunctionPot1", "Langevin_2potential_8hh.html#structparamFunctionPot1", null ],
    [ "paramFunctionStruct", "scripts_2langevin_2hh_2dynamic_8hh.html#structparamFunctionStruct", null ],
    [ "Particle", "classParticle.html", null ],
    [ "PhotonChannel", "classPhotonChannel.html", null ],
    [ "Proc_Struct", "structProc__Struct.html", null ],
    [ "SelectedProcess", "classSelectedProcess.html", null ],
    [ "Spallation", "scripts_2langevin_2hh_2HelperTemp_8hh.html#structSpallation", null ],
    [ "Spallation_l", "DataHelper_8hh.html#structSpallation__l", null ],
    [ "SpallationData", "structSpallationData.html", null ],
    [ "Tabela_a", "classTabela__a.html", null ],
    [ "TaskInput", "structTaskInput.html", null ],
    [ "TCanonical", "classTCanonical.html", null ],
    [ "TFragment", "classTFragment.html", null ],
    [ "Base", "classticpp_1_1Base.html", [
      [ "Attribute", "classticpp_1_1Attribute.html", null ],
      [ "Node", "classticpp_1_1Node.html", [
        [ "NodeImp< TiXmlComment >", "classticpp_1_1NodeImp.html", [
          [ "Comment", "classticpp_1_1Comment.html", null ]
        ] ],
        [ "NodeImp< TiXmlDeclaration >", "classticpp_1_1NodeImp.html", [
          [ "Declaration", "classticpp_1_1Declaration.html", null ]
        ] ],
        [ "NodeImp< TiXmlDocument >", "classticpp_1_1NodeImp.html", [
          [ "Document", "classticpp_1_1Document.html", null ]
        ] ],
        [ "NodeImp< TiXmlElement >", "classticpp_1_1NodeImp.html", [
          [ "Element", "classticpp_1_1Element.html", null ]
        ] ],
        [ "NodeImp< TiXmlStylesheetReference >", "classticpp_1_1NodeImp.html", [
          [ "StylesheetReference", "classticpp_1_1StylesheetReference.html", null ]
        ] ],
        [ "NodeImp< TiXmlText >", "classticpp_1_1NodeImp.html", [
          [ "Text", "classticpp_1_1Text.html", null ]
        ] ],
        [ "NodeImp< T >", "classticpp_1_1NodeImp.html", null ]
      ] ]
    ] ],
    [ "Exception", "classticpp_1_1Exception.html", null ],
    [ "Iterator< T >", "classticpp_1_1Iterator.html", null ],
    [ "TimeOrdering", "classTimeOrdering.html", null ],
    [ "TiXmlAttributeSet", "classTiXmlAttributeSet.html", null ],
    [ "TiXmlBase", "classTiXmlBase.html", [
      [ "TiXmlAttribute", "classTiXmlAttribute.html", null ],
      [ "TiXmlNode", "classTiXmlNode.html", [
        [ "TiXmlComment", "classTiXmlComment.html", null ],
        [ "TiXmlDeclaration", "classTiXmlDeclaration.html", null ],
        [ "TiXmlDocument", "classTiXmlDocument.html", null ],
        [ "TiXmlElement", "classTiXmlElement.html", null ],
        [ "TiXmlText", "classTiXmlText.html", null ],
        [ "TiXmlUnknown", "classTiXmlUnknown.html", null ]
      ] ]
    ] ],
    [ "TiXmlBase::Entity", "classTiXmlBase.html#structTiXmlBase_1_1Entity", null ],
    [ "TiXmlCursor", "structTiXmlCursor.html", null ],
    [ "TiXmlHandle", "classTiXmlHandle.html", null ],
    [ "TiXmlParsingData", "classTiXmlParsingData.html", null ],
    [ "TiXmlString", "classTiXmlString.html", [
      [ "TiXmlOutStream", "classTiXmlOutStream.html", null ]
    ] ],
    [ "TiXmlString::Rep", "classTiXmlString.html#structTiXmlString_1_1Rep", null ],
    [ "TiXmlVisitor", "classTiXmlVisitor.html", [
      [ "Visitor", "classticpp_1_1Visitor.html", null ],
      [ "TiXmlPrinter", "classTiXmlPrinter.html", null ]
    ] ],
    [ "TMakemcef", "classTMakemcef.html", null ],
    [ "TPartition", "classTPartition.html", null ],
    [ "TPartition::DeleteTFragment", "structTPartition_1_1DeleteTFragment.html", null ],
    [ "TPartitionHelper", "classTPartitionHelper.html", null ],
    [ "TRandomi", "classTRandomi.html", null ],
    [ "TSMM", "classTSMM.html", null ],
    [ "TStoragef", "classTStoragef.html", null ],
    [ "UnBindPart_Struct", "structUnBindPart__Struct.html", null ],
    [ "verletStruct", "scripts_2langevin_2propagateverlet_8hh.html#structverletStruct", null ],
    [ "Weisskopf", "classWeisskopf.html", null ],
    [ "work_rootFiles", "structwork__rootFiles.html", null ]
];